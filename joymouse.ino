#include "Mouse.h"
#include <EEPROM.h>

// set pin numbers for switch, joystick axes, and LED:
const int mouseLeftButton = 4;    // input pin for the mouse pushButton
const int mouseRightButton = 2;    // input pin for the mouse pushButton
const int mouseMiddleButton = 8;    // input pin for the mouse pushButton
const int scrollUpButton = 5; // input pin for scroll wheel up
const int scrollDownButton = 3; // input pin for scroll wheel down
const int dpi = 7; // sentivity
const int minMax = 6; // sentivity
const int xAxis = A0;         // joystick y axis
const int yAxis = A1;         // joystick x axis
const int dpiPin = 13;         // DPI status LED

// parameters for reading the joystick:
int senstivity[4] = {2, 4, 8, 10};
int range = senstivity[0];               // output range of X or Y movement
byte i = 0;
int responseDelay = 10;        // response delay of the mouse, in ms
int scrollDelay = 50;
float threshold = range / 4;    // resting threshold
float center = range / 2;       // resting position value
int scrollVal = 0;

void setDPI()
{
  switch(i)
    {
      case 0: analogWrite(dpiPin, 0); break;
      case 1: analogWrite(dpiPin, 84); break;
      case 2: analogWrite(dpiPin, 128); break;
      case 3: analogWrite(dpiPin, 255); break;
      default: i = 0; EEPROM.write(0, i); break;
    }
  range = senstivity[i];
  threshold = range / 4;    // resting threshold
  center = range / 2;       // resting position value
}

void setup() {
  // take control of the mouse:
  Mouse.begin();
  pinMode(dpiPin, OUTPUT);
  pinMode(mouseLeftButton, INPUT);
  pinMode(mouseRightButton, INPUT);
  pinMode(mouseMiddleButton, INPUT);
  pinMode(dpi, INPUT);
  pinMode(scrollUpButton, INPUT);
  pinMode(scrollDownButton, INPUT);
  i = EEPROM.read(0);
  setDPI();
}

void loop() {

  // read and scale the two axes:
  float xReading = readAxis(A1);
  float yReading = readAxis(A0);

  // read the mouse button and click or not click:
  // if the mouse button is pressed:
  if (digitalRead(mouseLeftButton) == LOW) {
    // if the mouse is not pressed, press it:
    if (!Mouse.isPressed(MOUSE_LEFT)) {
      Mouse.press(MOUSE_LEFT);
    }
  }
  // else the mouse button is not pressed:
  else {
    // if the mouse is pressed, release it:
    if (Mouse.isPressed(MOUSE_LEFT)) {
      Mouse.release(MOUSE_LEFT);
    }
  }

  if (digitalRead(mouseRightButton) == LOW) {
    // if the mouse is not pressed, press it:
    if (!Mouse.isPressed(MOUSE_RIGHT)) {
      Mouse.press(MOUSE_RIGHT);
    }
  }
  // else the mouse button is not pressed:
  else {
    // if the mouse is pressed, release it:
    if (Mouse.isPressed(MOUSE_RIGHT)) {
      Mouse.release(MOUSE_RIGHT);
    }
  }

  if (digitalRead(mouseMiddleButton) == LOW) {
    // if the mouse is not pressed, press it:
    if (!Mouse.isPressed(MOUSE_MIDDLE)) {
      Mouse.press(MOUSE_MIDDLE);
    }
  }
  // else the mouse button is not pressed:
  else {
    // if the mouse is pressed, release it:
    if (Mouse.isPressed(MOUSE_MIDDLE)) {
      Mouse.release(MOUSE_MIDDLE);
    }
  }

  if (digitalRead(scrollUpButton) == LOW) {
    // if the mouse is pressed, scroll up.
    scrollVal = 1;
    delay(scrollDelay);
  }
  else if (digitalRead(scrollDownButton) == LOW) {
    // if the mouse is pressed, scroll down
    scrollVal = -1;
    delay(scrollDelay);
  }
  else {
    scrollVal = 0;
  }

    // move the mouse:
  Mouse.move(xReading, yReading, scrollVal);

  if (digitalRead(dpi) == LOW) {
    i = (i==(sizeof(senstivity)/sizeof(int))-1) ? (0) : (i+1) ;
    EEPROM.write(0, i);
    setDPI();
    delay(500);
  }

  if (digitalRead(minMax) == LOW) {
    switch(i)
    {
      case 0: i = EEPROM.read(0); break;
      default: i = 0; break;
    }
    setDPI();
    delay(500);
  }

  delay(responseDelay);
}


float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
 return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


/*
  reads an axis (0 or 1 for x or y) and scales the analog input range to a range
  from 0 to <range>
*/

float readAxis(int thisAxis) {
  // read the analog input:
  float reading = analogRead(thisAxis);

  // map the reading from the analog input range to the output range:
  reading = mapfloat(reading, 0, 1023, 0, range);

  // if the output reading is outside from the rest position threshold, use it:
  float distance = reading - center;

  if (abs(distance) < threshold) {
    distance = 0;
  }

  // return the distance for this axis:
  return distance;
}